# SteamRadar

SteamRadar is a Discord Bot that uses data from the Steam API, website and related sources to display the information in the Discord Client.

## Adding SteamRadar

Although SteamRadar is Open Source, it is not suggested to self host this bot. The setup will require multiple tokens that are required for the bot to start. Instead it is recommended to simply add the already existing bot to your server.

To begin, make sure you have a Discord server in which you have at least the Manager Server permission. Anything higher such as Administrator or Owner of the server will also work.

Once you are sure that a server is available to add the bot you can add the bot by clicking [here](https://discordapp.com/oauth2/authorize?client_id=379109549514293248&scope=bot&permissions=0 "Bot Invite") or on the link below, and following the instructions provided by Discord.

#### Invite Link - [https://discordapp.com/oauth2/authorize?client_id=379109549514293248&scope=bot&permissions=0](https://discordapp.com/oauth2/authorize?client_id=379109549514293248&scope=bot&permissions=0 "Bot Invite")

## Built With

* [discord.js](https://github.com/discordjs/discord.js) - Discord API Library for JavaScript and Node
* [Discord Akairo](https://github.com/1Computer1/discord-akairo) - Command Framework
* [RethinkDB](https://github.com/rethinkdb/rethinkdb) - Database

## Contributing

If you wish to contribute to SteamRadar please contact me at `dsyomichev@gmail.com` or through my [discord server](https://discord.gg/KUbNQur).

## License

This project is licensed under the BSD3 License - see the [LICENSE.md](LICENSE.md) file for details
